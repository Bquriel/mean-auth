# MEAN Auth

## Requirements

If running with docker:
- Docker 18.0.0 and above
- Docker compose 1.24.0 and above
- Anguar CLI 10.0.0 and above

If running directly with node:
- NodeJS v14 and above
- MongoDB 4.0 and above
- Anguar CLI 10.0.0 and above

## Getting started

Choose in which mode you want to run the project.
Development mode supports module reloading and production mode offers better performance.

Configre relevant .env file in backend/ with your own parameters if necessary.

Open the prject in root folder
```bash
  ############## Docker ##############
  # Navigate to backend
  cd ./backend

  # Start docker
  docker-compose -f ./docker-compose.dev.yml up --build

  # NOTE! Initial startup may take a while and MongoDB default port should be free.

  # To start in production mode use
  docker-compose up --build

  ############## Localhost ##############
  # If you want to run project directly:
  # Have MongoDB installed locally and provide connection url to development.env.
  # Install packages
  npm ci

  # Start the server
  npm run dev
```

To build front end, or use it directly
Open project root in another terminal
```bash
  # Navigate to frontend
  cd ./frontend

  # Build frontend
  ng build

  # OR run directly
  ng serve --open
```

## Video and learning diary

Video demonstrating functionality of the application and learning diary is available [here](https://drive.google.com/drive/folders/1lsgqcxTT1C1PJK1aD8-oX9n_1OUimwwy?usp=sharing).


Usually .env files should not be located within the project! They are included here for easy testability. In ideal setup they could sit one level above the root folder outside of source control along with docker-compose files running multiple services or be provided by CI/CD pipeline.