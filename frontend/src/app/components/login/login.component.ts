import { Component } from '@angular/core';
import { Router } from "@angular/router";

import { FlashMessagesService } from "angular2-flash-messages";

import { AuthService } from '../../services/auth.service';


interface Credentials {
  username: string,
  password: string
};


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  formData: Credentials = {
    username: "",
    password: ""
  }

  constructor(
    private authService: AuthService,
    private flashMessage: FlashMessagesService,
    private router: Router
  ){ }

  async onLoginSubmit(): Promise<void> {
    try {
      const data: any = await this.authService.authenticateUser(this.formData).toPromise();

      this.authService.storeUserData( data.token, data.user );
      this.router.navigate(["/dashboard"]);
    } catch( err ){
      const message = err.error?.message ? err.error.message : "Something went wrong";
        this.flashMessage.show( message, { cssClass: "alert-danger", timeout: 3000 });
    }
  }
}
