import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";

import { FlashMessagesService } from "angular2-flash-messages";

import { AuthService } from '../../services/auth.service';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  user: object;

  constructor(private authService: AuthService, private flashMessage: FlashMessagesService, private router: Router) { }

  async ngOnInit(): Promise<void> {
    try {
      const data: any = await this.authService.getProfile().toPromise();

      this.user = data.user;
    } catch( err ){
      const message = err.error?.message ? err.error.message : err.error ? err.error : "Something went wrong";
        this.flashMessage.show( message, { cssClass: "alert-danger", timeout: 3000 });
    }
  }

}
