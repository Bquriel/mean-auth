import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";

import { FlashMessagesService } from "angular2-flash-messages";

import { ValidateService } from "../../services/validate.service";
import { AuthService } from '../../services/auth.service';

interface User {
  name: String,
  username: String,
  email: String,
  password: String
};

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  formData: User = {
    name: "",
    username: "",
    email: "",
    password: ""
  }

  constructor(
    private validateService: ValidateService,
    private flashMessage: FlashMessagesService,
    private authService: AuthService,
    private router: Router
    ){ }

  ngOnInit(): void {
  }

  async onRegisterSubmit(): Promise<boolean> {
    if( !this.validateService.validateRegister( this.formData )){
      this.flashMessage.show( "Please fill in all fields", { cssClass: "alert-danger", timeout: 3000});
      return false;
    }
    if( !this.validateService.validateEmail( this.formData.email )){
      this.flashMessage.show( "Please use a valid email", { cssClass: "alert-danger", timeout: 3000 });
      return false;
    }

    try {
      await this.authService.registerUser( this.formData ).toPromise();

      this.flashMessage.show( "Registration succesful", { cssClass: "alert-success", timeout: 3000 });
      this.router.navigate(["/login"]);
    } catch( err ){
      console.log( err );
      this.flashMessage.show( "Something went wrong", { cssClass: "alert-danger", timeout: 3000 });
      return false;
    }
    return true;
  }
}
