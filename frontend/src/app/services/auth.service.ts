import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  authToken: string;
  user: any;

  constructor(private http: HttpClient, private jwtHelper: JwtHelperService) { }

  registerUser(user){
    const headers = new HttpHeaders({"Content-Type": "application/json"});
    return this.http.post( "http://localhost:9013/v1/users/register", user, { headers });
  }

  authenticateUser(user){
    const headers = new HttpHeaders({"Content-Type": "application/json"});
    return this.http.post( "http://localhost:9013/v1/users/authenticate", user, { headers });
  }

  getProfile(){
    this.loadToken();
    const headers = new HttpHeaders({ "Content-Type": "application/json", "Authorization": this.authToken });
    return this.http.get( "http://localhost:9013/v1/users/profile", { headers });
  }

  loadToken(){
    this.authToken = localStorage.getItem( "id_token" );
  }

  storeUserData(token, user){
    localStorage.setItem( "id_token", token );
    localStorage.setItem( "user", JSON.stringify(user));
    this.authToken = token;
    this.user = user;
  }

  loggedIn(){
    return !this.jwtHelper.isTokenExpired();
  }

  logout(){
    this.authToken = null;
    this.user = null;
    localStorage.clear();
  }
}
