// Core node packages.
require( "events" ).EventEmitter.defaultMaxListeners = 25;
const { join }        = require( "path" );

// Globals.
global.__rootPath     = __dirname;
global.__accessEnv    = require( join( __rootPath, "util", "accessEnv" ));

if( __accessEnv( "CONTEXT", null ) === "localhost" ){
  try {
    const envfile = __accessEnv( "ENVFILE" );
    require( "dotenv" ).config({ path: join( __dirname, envfile )});
  } catch( err ){
    console.error( "ERROR, environment variable ENVFILE must be specified when running localhost!" );
    process.exit( 1 );
  }
}

// Third party packages.
const express         = require( "express" );
const helmet          = require( "helmet" );
const mongoose        = require( "mongoose" );
const passport        = require( "passport" );


// Own packages.
const allowCORS       = require( join( __rootPath, "middleware", "allowCORS" ));
const logger          = require( join( __rootPath, "util", "logger" ));
const passportJWT     = require( join( __rootPath, "middleware", "passportJWT" ));
const userService     = require( join( __rootPath, "services", "users" ));


const app = express();

// Middlewares.
app.use( allowCORS );
app.use( helmet());
app.use( express.json());
app.use( passport.initialize());
app.use( passport.session());
passport.use( passportJWT());


app.use( express.static( join( __dirname, "..", "static" )));
app.use( "/v1/users", userService );

// 404 controller.
app.use(( req, res, next ) => {
  res.sendFile( join( __rootPath, "..", "static", "index.html" ));
  // res.status( 404 ).json({ message: "404 Not found" });
});

//Errors given to next( error ) will go here.
app.use(( error, req, res, next ) => {
  logger.error( error );
  res.status( error.code || 500 ).json({ message: error.APIMessage || "Error" });
});

const db = mongoose.connection;

db.on( "connecting", () => {
  logger.info( "[SERVER] Connecting to database.." );
});

db.on( "error", err => {
  logger.error( "Error in MongoDb connection", err );
  mongoose.disconnect();
});

db.on( "disconnected", async () => {
  logger.error( "[SERVER] MongoDB disconnected!" );
  process.exit( 1 );
});

const dbUrl = __accessEnv( "MONGODB_URI" );

( async () => {
  try {
    const port = +__accessEnv( "PORT", 9012 );

    await mongoose.connect( dbUrl, {
      useNewUrlParser: true,
      poolSize: 15,
      useFindAndModify: false,
      useUnifiedTopology: true,
      useCreateIndex: true
    });

    await app.listen( port, "::" );
    logger.info( `Server listening on ${ port }` );
  } catch ( err ){
    logger.error( err );
    process.exit( 1 );
  }
})();

// process.on( "unhandledRejection", ( reason, promise ) => logger.error( "Unhandled Rejection at: Promise ", reason, promise ));