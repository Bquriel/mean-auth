module.exports = ( req, res, next ) => {
  res.setHeader( "Access-Control-Allow-Origin", "*" );                                                //Allows any domain to access data (CORS).
  res.setHeader( "Access-Control-Allow-Methods", "OPTIONS, HEAD, GET, POST, PUT, PATCH, DELETE" );    //Allows these methods for allowed origins.
  res.setHeader( "Access-Control-Allow-Headers", "Origin, X-RequestedW-With, Content-Type, Authorization, Accept" );
  next();
};