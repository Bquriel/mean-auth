const { join }        = require( "path" );

const JwtStrategy     = require( "passport-jwt" ).Strategy;
const ExtractJwt      = require( "passport-jwt" ).ExtractJwt;

const User            = require( join( __rootPath, "models", "user" ));

const key = __accessEnv( "JWT_SECRET" );


module.exports = () => {
  let opts = {};
  opts.jwtFromRequest = ExtractJwt.fromAuthHeaderWithScheme( "JWT" );
  opts.secretOrKey = key;
  return new JwtStrategy( opts, async ( payload, done ) => {
    try {
      const user = await User.findById( payload?.user._id );

      // If user is not found null will be returned.
      return done( null, user );
    } catch( err ){
      return done( err, false );
    }
  });
}