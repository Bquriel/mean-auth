const mongoose  = require( "mongoose" );
const bcrypt    = require( "bcryptjs" );

const hashRounds = +__accessEnv( "AUTHENTICATION_PASSWORD_HASH_ROUNDS" );


const schema = new mongoose.Schema({
  username: { type: String, required: true },
  email: { type: String, required: true, unique: true },
  name: { type: String },
  // role: { type: String, enum: Object.values( __types.roles ), default: __types.roles.REGULAR },
  password: { type: String, required: true }
}, { timestamps: true, autoIndex: true });


schema.statics.addUser = async function( user ){
  const password = await bcrypt.hash( user.password, hashRounds );
  return new this({ ...user, password }).save();
}

// This should ideally be instance method, but it wouldn't be registered when using .lean() which increases performance by great deal.
schema.statics.comparePassword = async function( password, hashedPassword ){
  return bcrypt.compare( password, hashedPassword );
}

const model =  mongoose.model( "User", schema );

setTimeout(() => model.createCollection(), 2000 );

module.exports = model;
