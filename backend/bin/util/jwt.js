const { promisify } = require( "util" );

const jwt           = require( "jsonwebtoken" );

const sign      = promisify( jwt.sign );
const jwtSecret = __accessEnv( "JWT_SECRET" );


exports.signJWT = async ( req, res, payload ) => {
    const oldToken = { ...req.token };
    delete oldToken.iat;
    delete oldToken.exp;
    const token = await sign({
        ...oldToken,
        ...payload
    }, jwtSecret, { expiresIn: "24h" });
    res.cookie( "authorization", token, { maxAge: 1000 * 60 * 60 * 24, httpOnly: true, sameSite: true });
    return token;
};