const cache = {};

// More performant way of using process.ENV.
module.exports = ( key, defaultVal ) => {
    if( !( key in process.env )){
        if( defaultVal ) return defaultVal;
        throw new Error( `${ key } not found in process.env!` );
    }

    if( cache[ key ]) return cache[ key ];

    cache[ key ] = process.env[ key ];

    return process.env[ key ];
}