const fs                = require( "fs" );
const { join }          = require( "path" );

const pino              = require( "pino" );

const isProduction = process.env.NODE_ENV  === "production";

let logger;
if( isProduction ){
    const dest = join( __rootPath, "..", "logs" );
    //Creates directory for logs not overwriting existing one.
    !fs.existsSync( dest ) && fs.mkdirSync( dest );

    logger = pino({}, pino.destination( join( __rootPath, "..", "logs", "errors.log" )));
} else {
    logger = pino({ prettyPrint: { colorize: true }}, pino.destination({
        sync: false // Asynchronous logging
    }));
};

const level = isProduction ? "error" : "debug";


module.exports = logger.child({ level });