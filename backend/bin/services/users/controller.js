const { join }    = require( "path" );

const { signJWT } = require( join( __rootPath, "util", "jwt" ));

const User        = require( join( __rootPath, "models", "user" ));


// /v1/users/profile => GET
exports.postRegister = async ( req, res, next ) => {
  const user = req.body;

  try {
    const newUser = await User.addUser( user );

    res.status( 201 ).json({ user: newUser });
  } catch( err ){
    err.APIMessage = "Error in registering";
    next( err );
  }
}

// /v1/users/register => POST
exports.postAuthenticate = async ( req, res, next ) => {
  const username = req.body.username;
  const password = req.body.password;

  try {
    const user = await User.findOne({ username }).lean();

    if( !user ){
      return res.status( 404 ).json({ message: "User not found" });
    }

    const match = await User.comparePassword( password, user.password );

    if( !match ){
      return res.status( 400 ).json({ message: "Incorrect username or password" });
    }

    delete user.password;

    const token = await signJWT( req, res, { user: { ...user, _id: user._id.toString()}});

    res.status( 200 ).json({ user, token: `JWT ${ token }` });
  } catch( err ){
    err.APIMessage = "Error in getting users";
    next( err );
  }
}

// /v1/users/authenticate => POST
exports.getProfile = async ( req, res, next ) => {
  try {
    res.status( 200 ).json({ user: req.user });
  } catch( err ){
    err.APIMessage = "Error in registering";
    next( err );
  }
}