const { join }    = require( "path" );

const express     = require( "express" );
const passport    = require( "passport" );

const controller  = require( join( __dirname, "controller" ));


const router = express.Router();

/****************************** Routes ******************************/
// /v1/users/profile => GET
router.get(   "/profile", passport.authenticate( "jwt", { session: false }),        controller.getProfile );

// /v1/users/register => POST
router.post(  "/register",                                                          controller.postRegister );

// /v1/users/authenticate => POST
router.post(  "/authenticate",                                                      controller.postAuthenticate );
/****************************** !Routes ******************************/

module.exports = router;